import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import net.sf.libosmscout.map 1.0
import Qt.labs.settings 1.0

import "custom"

Page {
	id: downloadPage
	anchors.fill: parent
    visible: false
	header: PageHeader {
		id: header
		title: "Available maps"
		flickable: downloadMapFlickable
		
		leadingActionBar {
			actions: [
				Action {
					iconName: "back"
					text: "Close"
					onTriggered: {
						map.reopenMap();
						downloadPage.visible = false;
						downloadPage.closed();
					}
				}
			]
		numberOfSlots: 1
		}
	}
	
	signal closed();
    
    Item {
        id: downloadPageItem
        anchors {
			fill: parent
			margins: units.gu(2)
		}
        
        property int currentFileIndex: -1;
        property string currentFile: "";
        
        function openConfirmDialog(index) {
            mapsModel.deleteIndex = index;
            confirmDialog.visible = true;
        }

        function showError(message) {
            errorDialog.text = message;
            errorDialog.visible = true;
        }
        
        function getNextFilename() {
            var filenames = [
                "md5sums",
                "areaarea.idx",
                "bounding.dat",
                "routebicycle.dat",
                "routefoot.idx",
                "waysopt.dat",
                "areanode.idx",
                "intersections.dat",
                "routebicycle.idx",
                "standard.oss",
                "areas.dat",
                "intersections.idx",
                "routecar.dat",
                "types.dat",
                "areasopt.dat",
                "location.idx",
                "routecar.idx",
                "water.idx",
                "areaway.idx",
                "nodes.dat",
                "routefoot.dat",
                "ways.dat"
            ];
            
            if(downloadPageItem.currentFileIndex < 21 && downloadPageItem.currentFileIndex >= -1) {
                downloadPageItem.currentFileIndex++;
                downloadPageItem.currentFile = filenames[downloadPageItem.currentFileIndex];
                return filenames[downloadPageItem.currentFileIndex];
            }
            else {
                downloadPageItem.currentFileIndex = -1;
                return "";
            }
        }
        
        Flickable {
			id: downloadMapFlickable
			anchors.fill: parent
			contentWidth: parent.width
			contentHeight: mainFrame.height
			flickableDirection: Flickable.VerticalFlick
			
			Settings {
				id: settings
				property int selectedmap: 0
			}

			MapListModel {
				id: mapsModel
				property int deleteIndex: 0
			}

			DownloadListModel {
				id: downloadsListModel
			}

			/*Component{
				id: confirmComponent

			}*/

			Column {
				id: mainFrame
				spacing: units.gu(1)
				width: parent.width
				
				Text {
					text: qsTr("Download Maps...")
				}
				
				Text {
					width: parent.width
					id: tAvMaps
					text: "<b>" + qsTr("Installed maps:") + "</b>"
				}

				ListView {
					id: mapsView
					model: mapsModel
					width: parent.width
					height: contentHeight
					interactive: false
					clip: true
					currentIndex: settings.selectedmap
						
					delegate: Rectangle {
						id: mapsItem
						width: parent.width
						height: downloadedItemColumn.height
						color: (index === mapsView.currentIndex ? "#fda" : (index % 2 === 0 ? "transparent" : "#efefef"))
						
						Column {
							id: downloadedItemColumn
							width: parent.width
							spacing: units.gu(0.25)
							
							Text {
								text: "<b>" + model.name + "</b>"
								width: parent.width
								wrapMode: Text.WordWrap
							}
							
							Text {
								text: model.path
								width: parent.width
								wrapMode: Text.WordWrap
							}
							
							Rectangle {
								width: parent.width
								height: 2
								color: "#ddd";
							}
						}
						
						MouseArea {
							anchors.fill: parent
							onClicked: {
								mapsView.currentIndex = index;
								settings.selectedmap = index;
								
								console.log("clicked", index);
							}
						}
					}
				}
				
				Button {
					id: deleteButton
					width: parent.width
					color: UbuntuColors.orange
					iconName: "delete"
					text: qsTr("Delete selected")
					onClicked: {
						downloadPageItem.openConfirmDialog(mapsView.currentIndex);
					}
				}

				Column {
					visible: false
					id: confirmDialog
					width: parent.width
					spacing: units.gu(0.5)
					/*Rectangle{
						anchors.fill: parent
						color: UbuntuColors.lightGrey
						radius: units.gu(1)
					}*/

					Label {
						text: qsTr("Delete") + " " + mapsModel.get(mapsModel.deleteIndex) + "?"
					}
					
					Button {
						width: parent.width
						color: UbuntuColors.orange
						text: qsTr("OK")
						onClicked: {
							console.log("Delete " + mapsModel.deleteIndex);
							if(mapsModel.deleteItem(mapsModel.deleteIndex)) {
								if(settings.selectedmap > mapsModel.deleteIndex) {
									settings.selectedmap--;
								}
							}
							freeSpace.text = mapsModel.getFreeSpace();
							confirmDialog.visible = false;
							mapsModel.refreshItems();
							mapsView.currentIndex = settings.selectedmap ? settings.selectedmap : 0;
						}
					}

					Button {
						width: parent.width
						text: qsTr("Cancel")
						onClicked: {
							confirmDialog.visible = false;
						}
					}
				}

				Label {
					id: freeSpace
					text: mapsModel.getFreeSpace()
				}

				Button {
					id: download
					text: qsTr("Download")
					visible: false
					width: parent.width
					onClicked: {
						errorDialog.visible = false;
						progressItem.visible = true;
						var name = downloadsListModel.get(downloadsView.currentIndex);
						downloadmanager.downloadFolder = mapsModel.getPreferredDownloadDir()+"/"+name;
						console.log("Download location: "+mapsModel.getPreferredDownloadDir()+"/"+name);
						download.enabled = false;
						downloadPageItem.currentFileIndex = -1;
						pause.visible = true;
						downloadmanager.download(downloadmanager.downloadUrl+name+"/"+downloadPageItem.getNextFilename(), downloadmanager.downloadFolder);
					}
				}
				
				Button {
					id: pause
					text: qsTr("Pause Download")
					width: parent.width
					visible: false
					onClicked:{
						if(text===qsTr("Pause Download")) {
							downloadmanager.pause();
							text = qsTr("Resume Download");
						}
						else {
							downloadmanager.resume();
							text = qsTr("Pause Download");
						}
					}
				}

				Item {
					id: progressItem
					visible: false;
					width: parent.width;
					height: progressBar.height+l1_1.height+l2_1.height+progressBarFiles.height
					
					Column {
						//height: parent.height
						width: parent.width
						
						Label {
							id: l1_1
							width: parent.width
							text: qsTr("Current file:")
						}
						
						ProgressBar {
							id: progressBar
							width: parent.width
							minimumValue: 0
							maximumValue: 100
							value: 0
						}
						
						Label {
							id: l2_1
							width: parent.width
							text: qsTr("Overall progress:")
						}
						
						ProgressBar {
							id: progressBarFiles
							width: parent.width
							minimumValue: 0
							maximumValue: 21
							value: downloadPageItem.currentFileIndex>=0?downloadPageItem.currentFileIndex:0
						}
					}
				}
				
				Label {
					id: errorDialog
					text: ""
					width: parent.width
					color: UbuntuColors.red
					visible: false
				}
				
				Label {
					text: "<b>"+qsTr("Select map to download:")+"</b>"
					width: parent.width
				}
				
				ListView {
					id: downloadsView
					model: downloadsListModel
					width: parent.width
					height: downloadPage.height / 2
					interactive: true
					clip: true
					delegate: Rectangle {
						id: mapsItem
						width: parent.width
						height: downloadItemColumn.height
						color: (index === downloadsView.currentIndex ? "#fda" : (index % 2 === 0 ? "transparent" : "#efefef"))
						
						Column {
							id: downloadItemColumn
							width: parent.width
							spacing: units.gu(0.25)
							
							Text {
								text: "<b>" + model.name + "</b>"
								width: parent.width
								wrapMode: Text.WordWrap
							}
							
							Text {
								text: model.size
								width: parent.width
								wrapMode: Text.WordWrap
							}
							
							Rectangle {
								width: parent.width
								height: 2
								color: "#ddd";
							}
						}
						
						MouseArea {
							anchors.fill: parent
							onClicked: {
								downloadsView.currentIndex = index;
								download.text = "Download " + downloadsListModel.get(index);
								download.visible = true;
							}
						}
					}
				}

				DownloadManager {
					property string downloadUrl: "http://schreuderelectronics.com/osm/";
					property string downloadFolder: "";
					id: downloadmanager
					onProgress: {
						progressBar.value = nPercentage;

					}
					onDownloadComplete: {
						freeSpace.text = mapsModel.getFreeSpace();
						if(downloadmanager.checkmd5sum(downloadFolder, downloadPageItem.currentFile)) {
							console.log("md5sum correct");
						}
						else {
							console.log("md5sum failed");
							downloadPageItem.showError(qsTr("md5sum of")+" "+downloadPageItem.currentFile+" "+qsTr("failed"));
							download.enabled = true;
							progressItem.visible = false;
							pause.visible = false;
							mapsModel.refreshItems();
							return;
						}
						if(downloadPageItem.currentFileIndex < 21) {
							var name = downloadsListModel.get(downloadsView.currentIndex);
							downloadmanager.download(downloadUrl+name+"/"+downloadPageItem.getNextFilename(), downloadFolder);
						}
						else {
							download.enabled = true;
							progressItem.visible = false;
							pause.visible = false;
							mapsModel.refreshItems();
						}
					}
				}
			}
        }
    }
}
